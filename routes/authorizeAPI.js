process.env.NODE_TLS_REJECT_UNAUTHORIZED='0';
var express = require('express');
const querystring = require('querystring');
const url = require('url');
var cors = require('cors');
const fetch = require("node-fetch");

var router = express.Router();

const corsOptions = {
    origin: 'null',
    credentials: true
};

const fetchTokenUrl = 'https://auth-klaptest.tfs.toyota.com/v1/auth/token';
const callBackUrl = 'https://portal-test.kintolink.com/authorization-code/callback';

async function fetchToken(url, reqAmcookie ) {
    try {
        const response = await fetch(url, {
            method: "POST",
            headers: {"Content-type": "application/json;charset=UTF-8"},
            body: reqAmcookie
        });
    
        console.log('Fetch Token', reqAmcookie);
        
        if (response.ok) {
            return await response.json();
        } else {
            return response.statusText;
        }
    } catch(err) {
       console.log("Error Server Side Access Token Failed with ", err);
    }
    return '';
};

router.get('/', cors(corsOptions), async function(req, res, next) {
    const amcookie = {
        amlbcookie:"",
        code:"",
        tokenId:"",
        successUrl: callBackUrl
    }
    let parsedUrl = url.parse(req.originalUrl);
    let parsedQs = querystring.parse(parsedUrl.query);
    console.log('parsedQs', parsedQs);
    const rCookie = req.headers.cookie;
    console.log('Header.Cookie', JSON.stringify(req.headers.cookie));
    console.log('Cookies: ', req.cookies);
    console.log('Query Param: ', req.query);
    console.log('Header Cookies: ', req.headers);
    // console.log('AMSession cookie', rCookie['AMSESSION'] );
    // console.log('amlbcookie cookie', req.cookie['amlbcookie'] );
    // console.log('dtCookie cookie', req.cookie['dtCookie'] );
    // var username = req.cookies['username'];
    rCookie && rCookie.split('; ').forEach(function( cookie ) {
        var parts = cookie.split('=');
        var part = parts.shift().trim();
        console.log("Cookie", parts);
        if( part == "AMSESSION" ) 
        {
         amcookie.tokenId =  parts.shift().trim();
        }
        if( part == "amlbcookie")
        {
            amcookie.amlbcookie = parts.shift().trim();
        }
    });
    amcookie.code = req.query.code;
    // const code = req.query.code;
    // res.send(code);
    console.log('API amCookie', amcookie);
    const reqAmcookie = JSON.stringify(amcookie);
    console.log('AmCookie', reqAmcookie);
    const result = await fetchToken(fetchTokenUrl, reqAmcookie);
    console.log("API Result", result);
    res.send(result);
    
    console.log('REquest', req);
});

module.exports = router;
